$(document).ready(function() {
$('#convert').on('click', function() {
var files=$('#files')[0].files;
if (!files) return;
for (var i=0; i<files.length; i++) {
    var file=files[i];
    fr = new FileReader();

    fr.onload = (function() {
            var filename=this.filename.substring(0, this.filename.lastIndexOf('.'));
            var note=$(this.result);
            var heading=note.find('.heading');
            var title=note.find('.title');
            var archived=note.find('.archived');
            var content=note.find('.content');

            var created=heading.text().trim();
            var subject=title.text();
            var isArchived=archived.length>0;
            var mdContent=content.html();
            
            mdContent=mdContent.replace(/<br>/g,"\n");
            mdContent=mdContent.replace(/<li class=\"listitem\">/g,"- ");
            var parsed=$.parseHTML(mdContent);
            var txtContent=$(parsed).text();

            var mdComplete="# "+ title.text() +"\n_"+heading.text().trim();
            if (isArchived) mdComplete+=" (archived)";
            mdComplete+="_\n\n"+txtContent;

            console.log(mdComplete);
            download(filename+".md", mdComplete);


    });
    fr.filename=file.name;
    fr.readAsText(file);
}


});
});


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);
  }
